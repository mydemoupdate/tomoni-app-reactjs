
import { Field, Form, Formik, FormikProps } from 'formik';

import React, { useEffect, useState } from 'react';

import {
    Card,
    CardBody,
    CardHeader,
    CardHeaderToolbar,
} from "../../../_metronic/_partials/controls";
// import '../../../assets/css/wizard.wizard-4.css';
import "../../../assets/css/wizard.wizard-4.css"
import '../../../assets/css/style-main.css'
import { Button, Row, Col } from "react-bootstrap";
import swal from 'sweetalert';
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../_metronic/_helpers";
import { useLocation } from "react-router";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { Input } from "../FieldFeedbackLabel/input";
import * as Yup from "yup";
import { Link } from "react-router-dom";
import { getProduct, updateProducts, createProducts } from "../../_redux_/productsSlice";
import { useDispatch } from "react-redux";

const unitId = [
    { label: "box" },
    { label: "slice" },
];

const originId = [
    { label: "ja" },
    { label: "vn" },
]
const ProductSchema = Yup.object().shape({
    name: Yup.string()
        .min(1, "Vui lòng nhập tên sản phẩm hợp lệ")
        .max(20, "Vui lòng nhập tên sản phẩm hợp lệ")
        .required("Vui lòng nhập tên sản phẩm"),
    price: Yup.number()
        .min(1, "Vui lòng nhập số tiền")
        .max(1000000, "$1000000 là lớn nhất")
        .required("Nhập giá"),
    origin_id: Yup.string()
        .min(1, "Vui lòng nhập ID hợp lệ")
        .max(20, "Vui lòng nhập ID hợp lệ")
        .required("Vui lòng nhập ID"),
    supplier_id: Yup.string()
        .min(1, "Vui lòng nhập tên sản phẩm hợp lệ")
        .max(20, "Vui lòng nhập tên sản phẩm hợp lệ")
        .required("Vui lòng nhập tên sản phẩm"),
    unit_id: Yup.string()
        .min(2, "Vui lòng nhập ID hợp lệ")
        .max(20, "Vui lòng nhập ID hợp lệ")
        .required("Vui lòng nhập ID"),
    id: Yup.string()
        .min(1, "Vui lòng nhập ID hợp lệ")
        .max(13, "Vui lòng nhập ID hợp lệ")
        .required("Vui lòng nhập ID"),
    locale: Yup.string()
        .min(1, "Vui lòng nhập locale hợp lệ")
        .max(20, "Vui lòng nhập locale hợp lệ")
        .required("Vui lòng nhập locale"),
    ingredients: Yup.string()
        .min(1, "Vui lòng nhập thành phần cho sản phẩm")
        .max(50, "Vui lòng nhập thành phần cho sản phẩm")
        .required("Vui lòng nhập thành phần cho sản phẩm"),

});


export function ProductCreate() {
    const ids = String(window.location.href).slice(44, 57);
    const [step, setStep] = useState(false)
    const [shipment, setShipment] = useState('');
    const [typeProduct, setTypeProduct] = useState();


    let location = useLocation();


    const product = getProduct(ids);
    console.log('product', product)

    useEffect(() => {
        if (location.pathname.includes('wholesale')) {
            setTypeProduct('wholesale');
        } else if (location.pathname.includes('auction')) {
            setTypeProduct("auction");
        } else if (location.pathname.includes('shipping')) {
            setTypeProduct("shippingpartner");
        } else if (location.pathname.includes('payment')) {
            setTypeProduct("paymentpartner");
        } else {
            setTypeProduct("retail");
        }
    }, [location]);
    const dispatch = useDispatch()
    var arrayIngre = []
    return (
        <div className="card card-custom card-transparent">
            {
                <Formik
                    initialValues={{ name: "", price: "", origin_id: "", supplier_id: "", unit_id: "", id: "", locale: "", ingredients: "" }}
                    validationSchema={ProductSchema}
                    onSubmit={async (values, actions) => {
                        await setTimeout(() => {
                            alert(JSON.stringify(values, null, 2));
                            console.log(values)
                            arrayIngre.push(values.ingredients)
                            const params = {
                                name: values.name,
                                price: values.price,
                                origin_id: values.origin_id,
                                supplier_id: values.supplier_id,
                                unit_id: values.unit_id,
                                id: values.id,
                                locale: values.locale,
                                ingredients: JSON.stringify(values.ingredients)
                            }
                            //    dispatch(createProducts(values.name, values.price, values.origin_id, values.supplier_id, values.unit_id, values.id,values.locale,values.ingredients))
                            dispatch(createProducts(params))
                            // actions.setSubmitting(false);
                            console.log("DONE1")
                        }, 500);
                        console.log("DONE2")
                    }}
                >
                    {(props) => (
                        <Form>
                            <Card>
                                <CardHeader title="Thêm sản phẩm mới">
                                    <CardHeaderToolbar>
                                        <Link
                                            type="button"
                                            className="btn btn-light"
                                            to={'/admin/product'}
                                        >
                                            <i className="fa fa-arrow-left"></i>
                                                     Trở về
                                                </Link>
                                        {`  `}

                                        <button type="submit"
                                            className="btn btn-primary ml-2"
                                        >
                                            Lưu
                                                </button>
                                    </CardHeaderToolbar>
                                </CardHeader>
                                <CardBody>
                                    <div className="form-group row mt-5">
                                        <div className="col-lg-3">
                                            <label>ID sản phẩm</label>
                                            <Field
                                                type="id"
                                                name="id"
                                                placeholder="ID Sẩn Phẩm"
                                                label=""
                                                component={Input}
                                            />
                                        </div>
                                        <div className="col-lg-3">
                                            <label>Tên sản phẩm VN</label>
                                            <Field
                                                type="name"
                                                name="name"
                                                placeholder="Tên sản phẩm VN"
                                                label=""
                                                component={Input}
                                            />
                                        </div>
                                        <div className="col-lg-3">
                                            <label>Giá tiền sẩn phẩm</label>
                                            <Field
                                                type="price"
                                                name="price"
                                                placeholder="Nhập số tiền "
                                                label=""
                                                component={Input}
                                            />
                                        </div>
                                        <div className="col-lg-3">
                                            <label>Thành phần sản phẩm</label>
                                            <Field
                                                type="ingredients"
                                                name="ingredients"
                                                placeholder="Thành phần sản phẩm"
                                                component={Input}
                                            />
                                        </div>
                                    </div>

                                    <div className="form-group row mt-5">
                                        <div className="col-lg-3">
                                            <label>ID Nơi xuất xứ</label>
                                            <Field
                                                type="origin_id"
                                                name="origin_id"
                                                placeholder="ID Nơi xuất xứ"
                                                label=""
                                                component={Input}
                                            />
                                        </div>
                                        <div className="col-lg-3">
                                            <label>ID Nhà cung cấp</label>
                                            <Field
                                                type="supplier_id"
                                                name="supplier_id"
                                                placeholder="ID Nhà cung cấp"
                                                label=""
                                                component={Input}
                                            />
                                        </div>
                                        <div className="col-lg-3">
                                            <label>Đơn vị</label>
                                            <Field
                                                type="unit_id"
                                                name="unit_id"
                                                placeholder="Đơn vị"
                                                component={Input}
                                            />
                                        </div>
                                        <div className="col-lg-3">
                                            <label>Nơi xuất xứ</label>
                                            <Field
                                                type="locale"
                                                name="locale"
                                                placeholder="Nơi xuất xứ"
                                                component={Input}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group row mt-5">

                                    </div>
                                </CardBody>
                            </Card>
                        </Form>
                    )}
                </Formik>
            }
        </div>
    );
}