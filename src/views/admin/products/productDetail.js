import { Field, Form, Formik, FormikProps } from 'formik';

import React, { useEffect, useState } from 'react';

import {
    Card,
    CardBody,
    CardHeader,
    CardHeaderToolbar,
} from "../../../_metronic/_partials/controls";
// import '../../../assets/css/wizard.wizard-4.css';
import "../../../assets/css/wizard.wizard-4.css"
import '../../../assets/css/style-main.css'
import { Button, Row, Col } from "react-bootstrap";
import swal from 'sweetalert';
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../_metronic/_helpers";
import { useLocation } from "react-router";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { Input } from "../FieldFeedbackLabel/input";
import * as Yup from "yup";
import { Link } from "react-router-dom";
import {getProduct, updateProducts} from "../../_redux_/productsSlice"


const unitId = [
  { label: "Box" },
  { label: "Slice" },
];

const originId = [
  { label: "ja" },
  { label: "vn" },
]
const ProductSchema = Yup.object().shape({
  price: Yup.number()
      .min(1, "Vui lòng nhập số tiền")
      .max(1000000, "$1000000 là lớn nhất")
      .required("Nhập giá"),
  id: Yup.string()
      .min(1, "Vui lòng nhập ID hợp lệ")
      .max(20, "Vui lòng nhập ID hợp lệ")
      .required("Vui lòng nhập ID"),
  name_VN: Yup.string()
      .min(1, "Vui lòng nhập tên sản phẩm hợp lệ")
      .max(20, "Vui lòng nhập tên sản phẩm hợp lệ")
      .required("Vui lòng nhập tên sản phẩm"),
  name_JA: Yup.string()
      .min(1, "Vui lòng nhập tên sản phẩm hợp lệ")
      .max(20, "Vui lòng nhập tên sản phẩm hợp lệ")
      .required("Vui lòng nhập tên sản phẩm"),
  tax_id: Yup.string()
      .min(1, "Vui lòng nhập mã số thuế hợp lệ")
      .max(20, "Vui lòng nhập mã số thuế hợp lệ")
      .required("Vui lòng nhập mã số thuế"),
  origin_id: Yup.string()
      .min(3, "Vui lòng nhập ID hợp lệ")
      .max(20, "Vui lòng nhập ID hợp lệ")
      .required("Vui lòng nhập ID"),
  ingredients_VN: Yup.string()
      .min(1, "Vui lòng nhập thành phần cho sản phẩm")
      .max(50, "Vui lòng nhập thành phần cho sản phẩm")
      .required("Vui lòng nhập thành phần cho sản phẩm"),
  ingredients_EN: Yup.string()
      .min(1, "Vui lòng nhập thành phần cho sản phẩm")
      .max(50, "Vui lòng nhập thành phần cho sản phẩm")
      .required("Vui lòng nhập thành phần cho sản phẩm")
});


export function ProductDetail() {
    const ids = String(window.location.href).slice(44,57);
    const [step, setStep] = useState(false)
    const [shipment, setShipment] = useState('');
    const [typeProduct, setTypeProduct] = useState();

    
    let location = useLocation();

    
    const product = getProduct(ids);
    console.log('product',product)
    
    useEffect(() => {
        if (location.pathname.includes('wholesale')) {
            setTypeProduct('wholesale');
        } else if (location.pathname.includes('auction')) {
            setTypeProduct("auction");
        } else if (location.pathname.includes('shipping')) {
            setTypeProduct("shippingpartner");
        } else if (location.pathname.includes('payment')) {
            setTypeProduct("paymentpartner");
        } else {
            setTypeProduct("retail");
        }
    }, [location]);
    return (
        <div className="card card-custom card-transparent">
            {
                <Formik
                    initialValues={{  price: "", id: "",name_VN: "",name_JA: "",tax_id: "",origin_id: "",ingredients_VN: "",ingredients_EN: "", isbox: false}}
                    validationSchema={ProductSchema}
                    onSubmit={(values, actions) => {
                        setTimeout(() => {
                            alert(JSON.stringify(values, null, 2));
                            actions.setSubmitting(false);
                        }, 1000);
                    }}
                >
                    {(props) => (
                        <Form>
                            <Card>
                                <CardHeader title="Thêm sản phẩm mới">
                                    <CardHeaderToolbar>
                                        <Link
                                            type="button"
                                            className="btn btn-light"
                                            to={'/admin/product'}
                                        >
                                            <i className="fa fa-arrow-left"></i>
                                                 Trở về
                                            </Link>
                                        {`  `}
                                        <button className="btn btn-light ml-2">
                                            <i className="fa fa-redo"></i>
                                                Làm mới
                                            </button>
                                        {`  `}
                                        <button type="submit"
                                            className="btn btn-primary ml-2">
                                            Lưu
                                            </button>
                                    </CardHeaderToolbar>
                                </CardHeader>
                                <CardBody>
                                    <div className="form-group row mt-5">
                                        <div className="col-lg-3">
                                            <label>ID sản phẩm</label>
                                            <Field
                                                type="id"
                                                name="id"
                                                placeholder="ID Sẩn Phẩm"
                                                label=""
                                                component={Input}
                                            />
                                        </div>
                                        <div className="col-lg-3">
                                            <label>Tên sản phẩm VN</label>
                                            <Field
                                                type="name.vn"
                                                name="name_VN"
                                                placeholder="Tên sản phẩm VN"
                                                label=""
                                                component={Input}
                                            />
                                        </div>
                                        <div className="col-lg-3">
                                            <label>Tên sản phẩm JA</label>
                                            <Field
                                                type="name.ja"
                                                name="name_JA"
                                                placeholder="Tên sản phẩm VN"
                                                label=""
                                                component={Input}
                                            />
                                        </div>
                                        <div className="col-lg-3">
                                            <label>Giá tiền sẩn phẩm</label>
                                            <Field
                                                type="price"
                                                name="price"
                                                placeholder="Nhập số tiền "
                                                label=""
                                                component={Input}
                                            />
                                        </div>
                                    </div>
                                    
                                    <div className="form-group row mt-5">
                                        <div className="col-lg-3">
                                            <label>ID Nhà cung cấp</label>
                                            <Field
                                                type="origin_id"
                                                name="origin_id"
                                                placeholder="ID Nhà cung cấp"
                                                label=""
                                                component={Input}
                                            />
                                        </div>
                                        <div className="col-lg-3">
                                            <label>Mã số thuế</label>
                                            <Field
                                                type="tax_id"
                                                name="tax_id"
                                                placeholder="Mã số thuế"
                                                label=""
                                                component={Input}
                                            />
                                        </div>
                                        <div className="col-lg-3">
                                            <label>Đơn vị</label>
                                            <Autocomplete
                                                options={unitId}
                                                autoHighlight
                                                getOptionLabel={option => option.label}
                                                renderOption={option => (
                                                    <React.Fragment>
                                                        {option.label}
                                                    </React.Fragment>
                                                )}
                                                renderInput={params => (
                                                    <TextField
                                                        {...params}
                                                        variant="outlined"
                                                        placeholder="Đơn vị"
                                                    />
                                                )}
                                            />
                                        </div>
                                        <div className="col-lg-3">
                                            <label>Nguồn gốc sản phẩm</label>
                                            <Autocomplete
                                                options={originId}
                                                autoHighlight
                                                getOptionLabel={option => option.label}
                                                renderOption={option => (
                                                    <React.Fragment>
                                                        {option.label}
                                                    </React.Fragment>
                                                )}
                                                renderInput={params => (
                                                    <TextField
                                                        {...params}
                                                        variant="outlined"
                                                        placeholder="Nguồn gốc sản phẩm"
                                                    />
                                                )}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group row mt-5">
                                        <div className="col-lg-3">
                                            <label>Thành phần sản phẩm VN</label>
                                            <Field
                                                type="ingredients_VN"
                                                name="ingredients_VN"
                                                placeholder="Thành phần sản phẩm VN"
                                                label=""
                                                component={Input}
                                            />
                                        </div>
                                        <div className="col-lg-3">
                                            <label>Thành phần sản phẩm EN</label>
                                            <Field
                                                type="ingredients_EN"
                                                name="ingredients_EN"
                                                placeholder="Thành phần sản phẩm EN"
                                                label=""
                                                component={Input}
                                            />
                                        </div>
                                        
                                    </div>
                                    <div className="form-group row">
                                    </div>
                                    <div className="form-group">
                                        <label>Mô tả thêm về sản phẩm</label>
                                        <Field
                                            name="description"
                                            as="textarea"
                                            className="form-control"
                                        />
                                    </div>
                                </CardBody>
                            </Card>
                        </Form>
                    )}
                </Formik>
            }
        </div>
    );
}