import React, { useEffect, useState } from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux'
import { getProductsList, deleteProducts } from '../../_redux_/productsSlice'
import BootstrapTable from "react-bootstrap-table-next";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../_metronic/_helpers";
import paginationFactory from 'react-bootstrap-table2-paginator';
import {
    Card,
    CardBody,
    CardHeader,
    CardHeaderToolbar,
} from "../../../_metronic/_partials/controls";
import { FormControl, InputGroup, OverlayTrigger, Tooltip, Form } from "react-bootstrap";
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import swal from 'sweetalert';
import { useLocation } from "react-router";
import { Link } from "react-router-dom";
import { id } from 'date-fns/locale';

export function ProductList() {

    const [typeSearch, setTypeSearch] = useState();
    const [typeProduct, setTypeProduct] = useState("Retail");
    const [params, setParams] = useState("");


    const onFindChange = (e) => {
        setTypeSearch(e.target.value);
    }
    const onKeySearch = (e) => {
        if (e.key === 'Enter') {
            setParams("search=director.type.id:" + typeProduct + ";" + e.target.value + "&searchJoin=and&page=1")
            if (typeSearch) {
                setParams("search=director.type.id:" + typeProduct + ";" + typeSearch + ":" + e.target.value + "&searchJoin=and&page=1");
            }
            dispatch(getProductsList(params));
            e.target.value = "";
        }
    }


    function deleteModal(object) { // React creates function whenever rendered
        swal({
            title: "Bạn có muốn xoá đơn " + object.id + " ?",
            icon: "warning",
            dangerMode: true,
            buttons: ["Huỷ", "Xoá"],
        })
            .then((willDelete) => {
                if (willDelete) {
                    dispatch(deleteProducts(object.id)).then(() => {
                        swal("Đã xoá thành công!", {
                            icon: "success",
                        });
                        setParams("search=" + typeProduct + "&searchFields=director.type.id&page=1");
                        dispatch(getProductsList());

                    }).catch((err) => {
                        swal("Xoá thất bại !", {
                            icon: "warning",
                        });
                    })
                }
            });
    }
    const getHandlerTableChange = (e) => { } // React creates function whenever rendered
    // Products UI Context
    const dispatch = useDispatch()
    let location = useLocation();

    useEffect(() => {
        if (location.pathname.includes('wholesale')) {
            setTypeProduct("Wholesale");
        } else if (location.pathname.includes('auction')) {
            setTypeProduct("Auction");
        } else if (location.pathname.includes('shipping')) {
            setTypeProduct("ShippingPartner");
        } else if (location.pathname.includes('payment')) {
            setTypeProduct("PaymentPartner");
        } else {
            setTypeProduct("Retail");
        }
        setParams("search=" + typeProduct + "&searchFields=director.type.id&page=1");

        dispatch(getProductsList("search=" + typeProduct + "&searchFields=director.type.id&page=1"));
    }, [location]);
    const { currentState } = useSelector(
        (state) => ({ currentState: state.products }),
        shallowEqual
    );

    const { totalCount, entities, perPage } = currentState;
    const columns = [
        {
            dataField: "id",
            text: "Mã Đơn",
            sort: true,
        },
        {
            dataField: "name.ja",
            text: "Tên SP",
            sort: true,
        },
        {
            dataField: "price",
            text: "Tiền ",
            sort: true,
        },
        {
            dataField: "origin_id",
            text: "Nguồn gốc ",
            sort: true,
        },
        {
            dataField: "supplier_id",
            text: "ID nhà CC ",
            sort: true,
        },
        {
            dataField: "unit_id",
            text: "Đơn vị ",
            sort: true,
        },
        {
            dataField: "tax_id",
            text: "Mã số thuế ",
            sort: true,
        },
        // {
        //     dataField: "ingredients.ja",           
        //     text: "Thành phần ",
        //     sort: true,
        // },
        // {
        //     dataField: "ingredients.en",
        //     text: "Thành phần ",
        //     sort: true,
        // },
        {
            dataField: "action",
            text: "Actions",
            classes: "text-right pr-0",
            headerClasses: "text-right pr-3",
            formatter: rankFormatter,
            style: {
                minWidth: "100px",
            },
        },
    ]

    function rankFormatter(cell, row, rowIndex, formatExtraData) {
        return (
            <>
                <OverlayTrigger
                    overlay={<Tooltip>Chỉnh sửa</Tooltip>}
                >

                    <Link to={`products/detail/${row.id}`}
                        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"

                    >
                        <span className="svg-icon svg-icon-md svg-icon-primary">
                            <SVG
                                src={toAbsoluteUrl("/media/svg/icons/General/Edit.svg")}
                            />
                        </span>
                    </Link>
                </OverlayTrigger>
                <>
                </>
                <OverlayTrigger
                    overlay={<Tooltip>Xoá Sản Phẩm </Tooltip>}
                >
                    <a
                        className="btn btn-icon btn-light btn-hover-danger btn-sm"
                        onClick={() => deleteModal(row)}
                    >
                        <span className="svg-icon svg-icon-md svg-icon-danger">
                            <SVG src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")} />
                        </span>
                    </a>
                </OverlayTrigger>
            </>
        );
    }
    const options = {
        hideSizePerPage: true,
        onPageChange: (page, sizePerPage) => {
            setParams("search=" + typeProduct + "&searchFields=director.type.id&page=" + page);
            dispatch(getProductsList(params));
        },
    };

    return (
        <>
            <Card>
                <CardHeader title="Danh sách sản phẩm">
                    <CardHeaderToolbar>
                        <Link to={'createproduct'}
                            type="button"
                            className="btn btn-primary"
                        ><i className="fa fa-plus"></i>
                                Thêm sản phẩm
                            </Link>

                    </CardHeaderToolbar>
                </CardHeader>
                <CardBody>
                    <div className="row">
                        <div className="col-6 pl-0">
                            <InputGroup>
                                <FormControl
                                    placeholder="Tìm kiếm"
                                    aria-label="Tìm kiếm"
                                    aria-describedby="basic-addon2"
                                    onKeyPress={onKeySearch}
                                />
                                <InputGroup.Append>
                                    <Form.Group>
                                        <Form.Control as="select" onChange={onFindChange}>
                                            <option value=''>Tìm theo</option>
                                            <option value='id'>Mã Sản Phẩm</option>
                                            <option value='name'>Tên sản phẩm</option>
                                        </Form.Control>
                                    </Form.Group>
                                </InputGroup.Append>
                            </InputGroup>
                        </div>
                    </div>

                    <div className="row mt-2">
                        <div className="col-12 pl-0">
                            <BootstrapTable
                                wrapperClasses="table-responsive"
                                classes="table table-head-custom table-vertical-center overflow-hidden"
                                remote
                                bordered={false}
                                keyField='id'
                                data={entities === null ? [] : entities.entities}
                                columns={columns}
                                onTableChange={getHandlerTableChange}
                                pagination={paginationFactory(options)}
                            />
                        </div>
                    </div>
                </CardBody>
            </Card>

        </>

    );
}


