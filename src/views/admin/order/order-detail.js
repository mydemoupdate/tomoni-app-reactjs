import React, {useEffect, useState} from 'react';
import {
    Card,
    CardBody,
    CardHeader,
    CardHeaderToolbar,
} from "../../../_metronic/_partials/controls";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import {FormControl,Button} from "react-bootstrap";
import { makeStyles } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';
import Icon from '@material-ui/core/Icon';
import {getOrderById,getProductList,updateItemOrder,getTaxesList} from '../../_redux_/ordersSlice'
import {useDispatch} from "react-redux";
import { useLocation} from "react-router";
import {useParams} from 'react-router-dom';
import {Link } from "react-router-dom";
import swal from "sweetalert";
import Select from "react-select";
const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    icon: {
        margin: theme.spacing(2),
    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: red[800],
        },
    },
}));


export function OrderDetail() {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [selectedValue, setSelectedValue] = useState();
    const [taxList, setTaxList] = useState([]);

    const [orderObject, setOrderObject] = useState({
        type: {id: ''},
        status: {name: {vi: ''}},
        cost: {},
        items: [{
            amount: 0
        }]
    });

    const columns = [
        {
            dataField: "id",
            text: "Mã đơn ",
            sort: true,
        },
        {
            dataField: "name",
            text: "Khách ",
            sort: true,
        }
    ]
   const arr = [
        {
            id: 1,
            name: 'name'
        }
    ]
    const { id } = useParams()
    let location = useLocation();
    useEffect(() => {
        dispatch(getTaxesList()).then(res=>{
            const _data = res.data || [];
            var result = [];
            for(var i = 0;i<_data.length;i++){
                result.push({
                    'value': _data[i].percent,
                    'label': _data[i].percent
                })
            }
            // setSelectedValue(_data[0].percent)
            setTaxList(result);
        })
        dispatch(getOrderById(id)).then(res=>{
            const object = res.data || {};
            var productID = [];
            var objectItems = object.items || [];
            for(var i=0 ;i<objectItems.length;i++){
                productID.push(object.items[i].product_id);
            }
            console.log(object)
            if(objectItems.length>0){
                dispatch(getProductList(productID.join(';'))).then(response=>{
                    const _data = response.data.data || [];
                    objectItems.forEach((value,i) => {
                        _data.forEach(product => {
                            if(value.product_id === product.id){
                                if(product.name.ja){
                                    value['name']= product.name.ja;
                                }else if(product.name.en){
                                    value['name']= product.name.en;
                                }else {
                                    value['name']= product.name.vi;
                                }
                            }

                        })
                        value.tax_percent = Number(value.tax_percent);
                        console.log(value)
                        if(i===objectItems.length-1){


                            setOrderObject(res.data);
                        }
                    })
                })
            }else {
                objectItems.forEach((value,i) => {
                    console.log(Number(value.tax_percent))
                    value.tax_percent = Number(value.tax_percent);
                    if(i===objectItems.length-1){
                        setOrderObject(res.data);
                    }
                })
            }
        })
    },[dispatch,location])

    const updatePrice= (e,item)=> {
        if (e.key === 'Enter') {
            console.log(e.target.value+'   ', item)
            const parms = {
                id: item.id,
                price: e.target.value
            }
            dispatch(updateItemOrder(parms)).then(()=>{
                swal("Đã thay đổi giá!", {
                    icon: "success",
                });
            })
        }
    }
    const updateQuantity= (e,item)=> {
        if (e.key === 'Enter') {
            const parms = {
                id: item.id,
                quantity: e.target.value
            }
            dispatch(updateItemOrder(parms)).then(()=>{
                swal("Đã thay đổi số lượng!", {
                    icon: "success",
                });
            })
        }
    }
    // handle onChange event of the dropdown
    const handleChange = (e) => {
        console.log(e)
        e.value = e.value
    }
    return (
        <Card>
            <CardHeader title="Chi tiết đơn hàng">
                <CardHeaderToolbar>
                    <Link
                        type="button"
                        to={'/admin/order-'+orderObject.type.id.toLowerCase()}
                        className="btn btn-light"
                    >
                        <i className="fa fa-arrow-left"></i>
                        Trở về
                    </Link>
                </CardHeaderToolbar>
            </CardHeader>
            <CardBody>
                <div className="col">
                    <div className="float-left">
                        <p className="mb-2 font-weight-bold">
                            Đơn hàng:
                            <span className="text-primary font-size-lg"> #{orderObject.id}</span>
                        </p>
                        <p className="mb-2 font-weight-bold">
                            Trạng thái:
                            <span className="badge badge-pill badge-success ml-2"
                            > {orderObject.status.name.vi}</span>
                        </p>
                        <p className="mb-2 font-weight-bold">
                            Khách hàng:
                            <span className="text-primary"> {orderObject.customer_id}</span>
                        </p>

                    </div>
                    {
                        orderObject.type.id === 'PaymentPartner' || orderObject.type.id === 'ShippingPartner'?
                            <div className="float-right">
                                <div className={classes.root}>
                                    <Icon className={classes.icon} color="primary" fontSize="large">
                                        add_circle
                                    </Icon>
                                </div>
                            </div>:''
                    }
                </div>
            </CardBody>
            <CardBody>
                <div className="table-responsive table-bordered">
                    <table className="table mb-0">
                        <thead>
                        <tr>
                            <th>Jancode</th>
                            <th>Sản phẩm</th>
                            <th>Giá</th>
                            <th>Số lượng</th>
                            <th>Thuế</th>
                            <th>Tổng tiền</th>

                            <th>Tracking</th>
                        </tr>
                        </thead>
                        <tbody>
                        {orderObject.items.map((item, i)=>
                            <tr key={i}>
                                <td>{item.product_id}</td>
                                <td width="30%">{item.name}</td>
                                <td width="12%">
                                    <FormControl type="number" defaultValue={item.price}  onChange={()=>{}} onKeyPress={(e)=>updatePrice(e,item)}/>
                                </td>
                                <td width="10%">
                                    <FormControl type="number" defaultValue={item.quantity} onChange={(e)=>{}} onKeyPress={(e)=>updateQuantity(e,item)}/>
                                </td>
                                <td>
                                    {/*<select  defaultValue={item.tax_percent}>*/}
                                    {/*    {taxList.map(object =>*/}
                                    {/*        <option key={object.value} value={object.value} onChange={(e)=>{handleChange(e)}}>{object.label}</option>*/}
                                    {/*    )}*/}
                                    {/*</select>*/}
                                    {/*const [selectedValue, setSelectedValue] = useState(0);*/}
                                    <Select
                                        options={taxList}
                                        value={taxList.filter(obj => obj.value == item.tax_percent)}
                                        onChange={(e)=>{handleChange(e)}}
                                    />
                                </td>
                                <td>{item.amount.toLocaleString()}</td>

                                <td className="text-center">
                                    {
                                        item.tracking? item.tracking.map((track,i)=>
                                            <div key={i}>
                                                <span className="badge badge-success">{track.id}</span>
                                            </div>
                                        ):''
                                      }


                                </td>
                            </tr>
                        )}


    <tr>
        <th scope="col" colSpan="6" className="text-right">Thuế</th>
        <th className="font-weight-bold text-center">{orderObject.cost.tax}</th>

    </tr>
    <tr>
        <th scope="col" colSpan="6" className="text-right">Số dư</th>
        <th className="font-weight-bold text-center">{orderObject.cost.balance}</th>

    </tr>
    <tr>

        <th colSpan="6" className="text-right">Chiết khấu</th>
        <th className="font-weight-bold text-center">{orderObject.cost.discount_tax_percent}</th>
    </tr>
    <tr>

        <th colSpan="6" className="text-right">Tổng</th>
        <th className="font-weight-bold text-center">{orderObject.cost.sub_total}</th>
    </tr>
    <tr>
        <th colSpan="7">
            <Button variant="success" >Sửa đơn</Button>
        <Button  className="ml-2 mr-2" >Nhật ký đơn</Button>
    <Button className="mr-2">Phát sinh gao dịch</Button>
    <Button>Đơn vận chuyển</Button>
</th>
</tr>
</tbody>
</table>
</div>
            </CardBody>
        </Card>

    );
}


