import React, { useEffect, useState } from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux'
import { getOrderList, deleteOrder } from '../../_redux_/ordersSlice'
import BootstrapTable from "react-bootstrap-table-next";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../_metronic/_helpers";
import paginationFactory from 'react-bootstrap-table2-paginator';
import {
    Card,
    CardBody,
    CardHeader,
    CardHeaderToolbar,
} from "../../../_metronic/_partials/controls";
import { FormControl, InputGroup, OverlayTrigger, Tooltip, Form } from "react-bootstrap";
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import swal from 'sweetalert';
import { useLocation } from "react-router";
import { Link } from "react-router-dom";

export function OrderList() {

    const [typeSearch, setTypeSearch] = useState();
    const [typeOrder, setTypeOrder] = useState();
    const [params, setParams] = useState();


    const onFindChange = (e) => {
        setTypeSearch(e.target.value);
    }
    const onKeySearch = (e) => {
        if (e.key === 'Enter') {

            if(typeSearch){
                dispatch(getOrderList("search=director.type.id:"+typeOrder+";"+typeSearch+":"+e.target.value+"&searchJoin=and&page=1"));
            }else {
                dispatch(getOrderList("search=director.type.id:"+typeOrder+"&search="+e.target.value+"&searchJoin=and&page=1"));
            }
            e.target.value="";

        }
    }


    function deleteModal(object) { // React creates function whenever rendered
        swal({
            title: "Bạn có muốn xoá đơn " + object.id + " ?",
            icon: "warning",
            dangerMode: true,
            buttons: ["Huỷ", "Xoá"],
        })
            .then((willDelete) => {
                if (willDelete) {
                    dispatch(deleteOrder(object.id)).then(() => {
                        swal("Đã xoá thành công!", {
                            icon: "success",
                        });
                        setParams("search=" + typeOrder + "&searchFields=director.type.id&page=1");
                        dispatch(getOrderList(params));

                    }).catch((err) => {
                        swal("Xoá thất bại !", {
                            icon: "warning",
                        });
                    })
                }
            });
    }
    const getHandlerTableChange = (e) => { } // React creates function whenever rendered
    // Products UI Context
    const dispatch = useDispatch()
    let location = useLocation();

    useEffect(() => {

        if(location.pathname.includes('wholesale')){
            setTypeOrder('wholesale');
        }else if(location.pathname.includes('auction')){
            setTypeOrder("auction");
        }else if (location.pathname.includes('shipping')){
            setTypeOrder("shippingpartner");
        }else if (location.pathname.includes('payment')){
            setTypeOrder("paymentpartner");
        }else{
            setTypeOrder("retail");
        }
        setParams("search="+typeOrder+"&searchFields=director.type.id&page=1");
        console.log(typeOrder,'   parms:  ',params);
        dispatch(getOrderList("search="+typeOrder?typeOrder:'retail'+"&searchFields=director.type.id&page=1"));

    }, [typeOrder,dispatch,location]);

    const { currentState } = useSelector(
        (state) => ({ currentState: state.orders }),
        shallowEqual
    );


    const {entities }= currentState;

    const columns = [
        {
            dataField: "id",
            text: "Mã đơn ",
            sort: true,
        },
        {
            dataField: "customer_id",
            text: "Khách ",
            sort: true,
        },
        {
            dataField: "shipment_method_id",
            text: "Vận chuyển ",
            sort: true,
            formatter: shipFormatter,
        },
        {
            dataField: "action",
            text: "Actions",
            classes: "text-right pr-0",
            headerClasses: "text-right pr-3",
            formatter: rankFormatter,
            style: {
                minWidth: "100px",
            },
        },
    ]

    function shipFormatter(cell, row){
        return (
            <>
                <span className={row.shipment_method_id==='sea'?'badge badge-pill badge-info':'badge badge-pill badge-secondary'}>
                    {row.shipment_method_id==='sea'?'Đường biển':'Đường bay'}
                </span>
            </>
        )
    }

    function rankFormatter(cell, row, rowIndex, formatExtraData) {
        return (
            <>
                <OverlayTrigger
                    overlay={<Tooltip>Xem chi tiết</Tooltip>}
                >

                    <Link to={`orders/${row.id}`}
                        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                    >
                        <span className="svg-icon svg-icon-md svg-icon-primary">
                            <SVG
                                src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                            />
                        </span>
                    </Link>
                </OverlayTrigger>
                <>
                </>
                <OverlayTrigger
                    overlay={<Tooltip>Xoá </Tooltip>}
                >
                    <a
                        className="btn btn-icon btn-light btn-hover-danger btn-sm"
                        onClick={() => deleteModal(row)}
                    >
                        <span className="svg-icon svg-icon-md svg-icon-danger">
                            <SVG src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")} />
                        </span>
                    </a>
                </OverlayTrigger>
            </>
        );
    }

    const options = {
        hideSizePerPage: true,
        onPageChange: (page, sizePerPage) => {
            setParams("search="+typeOrder+"&searchFields=director.type.id&page="+page);
            dispatch(getOrderList("search="+typeOrder+"&searchFields=director.type.id&page="+page));

        },
    };

    return (
        <>

        <Card>
            <CardHeader title="Danh sách đơn hàng">
                <CardHeaderToolbar>
                    {
                        typeOrder!=='retail'?
                            <Link to={'create-'+typeOrder}
                                type="button"
                                className="btn btn-primary"
                            ><i className="fa fa-plus"></i>
                                Tạo đơn
                            </Link> : ''
                        }
                    </CardHeaderToolbar>
                </CardHeader>
                <CardBody>
                    <div className="row">
                        <div className="col-6 pl-0">
                            <InputGroup>
                                <FormControl
                                    placeholder="Từ khoá tìm kiếm"
                                    aria-label="Từ khoá tìm kiếm"
                                    aria-describedby="basic-addon2"
                                    onKeyPress={onKeySearch}
                                />

                                <InputGroup.Append>
                                    <Form.Group>
                                        <Form.Control as="select" onChange={onFindChange}>
                                            <option value=''>Tìm theo</option>
                                            <option value='id'>Mã đơn hàng</option>
                                            <option value='customer_id'>Khách hàng</option>
                                            <option value='air'>Vận chuyển đường bay</option>
                                            <option value='sea'>Vận chuyển đường biển</option>
                                        </Form.Control>
                                    </Form.Group>
                                </InputGroup.Append>
                            </InputGroup>
                        </div>
                    </div>

                    <div className="row mt-2">
                        <div className="col-12 pl-0">
                            <BootstrapTable
                                wrapperClasses="table-responsive"
                                classes="table table-head-custom table-vertical-center overflow-hidden"
                                remote
                                bordered={false}
                                keyField='id'
                                data={entities === null ? [] : entities.entities}
                                columns={columns}
                                onTableChange={getHandlerTableChange}
                                pagination={paginationFactory(options)}
                            />
                        </div>
                    </div>
                </CardBody>
            </Card>

        </>

    );
}


