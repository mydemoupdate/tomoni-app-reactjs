
import React, {Suspense, lazy} from "react";
import {Redirect, Switch, Route} from "react-router-dom";
import {LayoutSplashScreen, ContentRoute} from "../../_metronic/layout";
import {DashboardPage} from "../../app/pages/DashboardPage";
import {OrderList} from "./order/order-list";
import {OrderDetail} from "./order/order-detail";
import {CreateOrder} from "./order/create-order";
import {ProductList} from "./products/productList";
import {ProductDetail} from "./products/productDetail";
import {ProductCreate} from "./products/productCreate"
export default function AdminRoutes() {

    return (
        <Suspense fallback={<LayoutSplashScreen/>}>
            <Switch>
                {
                    /* Redirect from root URL to /dashboard. */
                    <Redirect exact from="/" to="/admin"/>
                }
                {/*<Route path="/admin" component={AdminRoutes}/>*/}
                <ContentRoute path="/admin/orders/:id" component={OrderDetail}/>
                <ContentRoute path="/admin/order-retail" component={OrderList}/>
                <ContentRoute path="/admin/list" component={OrderList}/>
                <ContentRoute path="/admin/order-wholesale" component={OrderList}/>
               
                <ContentRoute path="/admin/order-auction" component={OrderList}/>
                <ContentRoute path="/admin/order-shipping" component={OrderList}/>
                <ContentRoute path="/admin/order-payment" component={OrderList}/>
                <ContentRoute path="/admin/create-wholesale" component={CreateOrder}/>
              <ContentRoute path="/admin/create-auction" component={CreateOrder}/>
               <ContentRoute path="/admin/create-shippingpartner" component={CreateOrder}/>
               <ContentRoute path="/admin/create-paymentpartner" component={CreateOrder}/>
                <ContentRoute path="/admin/product" component={ProductList}/>
                <ContentRoute path="/admin/products/detail/:id" component={ProductDetail}/>
                <ContentRoute path="/admin/createproduct" component={ProductCreate}/>
                <ContentRoute path="/admin" component={DashboardPage}/>
            </Switch>
        </Suspense>
    );
}
