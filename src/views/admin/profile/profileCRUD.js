export const GET_USER_URL =
  "http://139.180.207.4:81/api/me?with=roles;directPermissions";
export const CHANGE_PASSWORD_URL = "http://139.180.207.4:81/api/me/password";
export function getUser() {
  return fetch(GET_USER_URL, {
    method: "get",
    headers: new Headers({
      Authorization:
        "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZTQ1ODU5MDc3ODI0Njg2YzU4Y2JiYjQ4MGQ3YzYwMDhkYzA3MWU3ZDdlMGQ0YTE3Mzc5NzhmMzgxYWFiMjQyNjJmN2E5N2VhMjgyYzU2NDMiLCJpYXQiOjE1OTY2ODkzMzUsIm5iZiI6MTU5NjY4OTMzNSwiZXhwIjoxNTk3OTg1MzM1LCJzdWIiOiJhZG1pbiIsInNjb3BlcyI6WyIqIl19.E3Thh3X6FVijA_KQGcOQCIuwafiuohz-DPR8tRa8TWpWz5ffDFjJItNf_UbrPn7BTigxQ2VfsnTwAPqgibqAoa_TYdqYjZUmkOfg79JsB7T-PSUf0LnajY2qx_ywaQITIs2bkSQWxl9HjEDf9fnXvqCER-Q1N70qXUWG4UIHldUKQkqa1J_cgNtFjFsmqz4N7XDqtB5Mp-sopLrLMcSeqT2xFM9buT-LI1EXTnjVTnJyFs_fWgCX8HttvyLdPMrO3afxzuBf0HH3EiPVPPCXz7nKah41j8WqJWI6FDmPUcsO7P_dTzVXnvT7ZB5fq5NpKLzJ-rSYn_p1phv4ubXKWp9bb7A-ACrwer1y3RGIM7Q_eHSqkMibxNiNohfbwoQc3beAgSBPsB4t5JylrWSIh2pNvziO8ghov-HfCIErGMZos5DaXGo6DISdIL9NE1SyWPTtM7b68jLV7ZbGv0SbI-QRWGaopHDIZu9gVRr1dq8cPwnF2UHXoZzoXkxD_WrDdA-dyLpWtg5gRuIy0BgvU38ujvmLZMy6Skho2M-3RjVA7ReR7okP38ghlLRzgr5aygd3RXxTHydCXptRCWwzpk382txkYGZ_P-wzCWz0drA_SCaG2t0mGXP3nFmbgKESUR1L82O54DxH5Q_PrbD2O_IHP3cf9-UYbE31Amunw-0",
    }),
  });
}
