import React, { useState } from "react";
import CancelIcon from "@material-ui/icons/Cancel";

function ChangePassword() {
  const password = useFormInput("");
  const password_confirmation = useFormInput("");
  const handleChangePass = () => {
    var myHeaders = new Headers();
    myHeaders.append("Accept", "application/json");
    myHeaders.append(
      "Authorization",
      "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZTQ1ODU5MDc3ODI0Njg2YzU4Y2JiYjQ4MGQ3YzYwMDhkYzA3MWU3ZDdlMGQ0YTE3Mzc5NzhmMzgxYWFiMjQyNjJmN2E5N2VhMjgyYzU2NDMiLCJpYXQiOjE1OTY2ODkzMzUsIm5iZiI6MTU5NjY4OTMzNSwiZXhwIjoxNTk3OTg1MzM1LCJzdWIiOiJhZG1pbiIsInNjb3BlcyI6WyIqIl19.E3Thh3X6FVijA_KQGcOQCIuwafiuohz-DPR8tRa8TWpWz5ffDFjJItNf_UbrPn7BTigxQ2VfsnTwAPqgibqAoa_TYdqYjZUmkOfg79JsB7T-PSUf0LnajY2qx_ywaQITIs2bkSQWxl9HjEDf9fnXvqCER-Q1N70qXUWG4UIHldUKQkqa1J_cgNtFjFsmqz4N7XDqtB5Mp-sopLrLMcSeqT2xFM9buT-LI1EXTnjVTnJyFs_fWgCX8HttvyLdPMrO3afxzuBf0HH3EiPVPPCXz7nKah41j8WqJWI6FDmPUcsO7P_dTzVXnvT7ZB5fq5NpKLzJ-rSYn_p1phv4ubXKWp9bb7A-ACrwer1y3RGIM7Q_eHSqkMibxNiNohfbwoQc3beAgSBPsB4t5JylrWSIh2pNvziO8ghov-HfCIErGMZos5DaXGo6DISdIL9NE1SyWPTtM7b68jLV7ZbGv0SbI-QRWGaopHDIZu9gVRr1dq8cPwnF2UHXoZzoXkxD_WrDdA-dyLpWtg5gRuIy0BgvU38ujvmLZMy6Skho2M-3RjVA7ReR7okP38ghlLRzgr5aygd3RXxTHydCXptRCWwzpk382txkYGZ_P-wzCWz0drA_SCaG2t0mGXP3nFmbgKESUR1L82O54DxH5Q_PrbD2O_IHP3cf9-UYbE31Amunw-0"
    );

    var formdata = new FormData();
    formdata.append("password", password.value);
    formdata.append("password_confirmation", password_confirmation.value);

    var requestOptions = {
      method: "PUT",
      headers: myHeaders,
      redirect: "follow",
    };

    fetch(
      `http://139.180.207.4:81/api/me/password?password=${password.value}&password_confirmation=${password_confirmation.value}`,
      requestOptions
    )
      .then((response) => response.json())
      .then((result) => {
        if (result.message !== undefined) alert(result.message);
        else alert("Your password has been changed");
      })
      .catch((error) => alert(error));
  };
  return (
    <div class="flex-row-fluid ml-lg-8">
      <div class="card card-custom">
        <div class="card-header py-3">
          <div class="card-title align-items-start flex-column">
            <h3 class="card-label font-weight-bolder text-dark">
              Change Password
            </h3>
            <span class="text-muted font-weight-bold font-size-sm mt-1">
              Change your account password
            </span>
          </div>
          <div class="card-toolbar">
            <button
              type="button"
              class="btn btn-success mr-2"
              onClick={handleChangePass}
            >
              Save Changes
            </button>
            <button type="reset" class="btn btn-secondary">
              Cancel
            </button>
          </div>
        </div>

        <form class="form">
          <div class="card-body">
            <div
              class="alert alert-custom alert-light-danger fade show mb-10"
              role="alert"
            >
              <div class="alert-icon">
                <span class="svg-icon svg-icon-3x svg-icon-danger">
                  <CancelIcon style={{ color: "pink" }}></CancelIcon>
                </span>{" "}
              </div>
              <div class="alert-text font-weight-bold">
                Configure user passwords to expire periodically. Users will need
                warning that their passwords are going to expire,
                <br />
                or they might inadvertently get locked out of the system!
              </div>
              <div class="alert-close">
                <button
                  type="button"
                  class="close"
                  data-dismiss="alert"
                  aria-label="Close"
                >
                  <span aria-hidden="true">
                    <i class="ki ki-close"></i>
                  </span>
                </button>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-xl-3 col-lg-3 col-form-label text-alert">
                New Password
              </label>
              <div class="col-lg-9 col-xl-6">
                <input
                  type="password"
                  class="form-control form-control-lg form-control-solid"
                  placeholder="New password"
                  {...password}
                />
              </div>
            </div>
            <div class="form-group row">
              <label class="col-xl-3 col-lg-3 col-form-label text-alert">
                Confirmation Password
              </label>
              <div class="col-lg-9 col-xl-6">
                <input
                  type="password"
                  class="form-control form-control-lg form-control-solid"
                  placeholder="Verify password"
                  {...password_confirmation}
                />
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default ChangePassword;
const useFormInput = (initialValue) => {
  const [value, setValue] = useState(initialValue);

  const handleChange = (e) => {
    setValue(e.target.value);
  };
  return {
    value,
    onChange: handleChange,
  };
};
