import React, { Component } from "react";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import { getUser } from "../profileCRUD";

class ProfileInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      email: "",
      name: "",
    };
  }
  componentDidMount() {
    getUser()
      .then((res) => res.json())
      .then((data) => {
        this.setState({
          email: data.email,
          id: data.id,
          name: data.roles["0"].name,
        });
      });
  }
  render() {
    console.log(this.state);
    return (
      <div>
        <div class="flex-row-fluid ml-lg-8">
          <div class="card card-custom card-stretch">
            <div class="card-header py-3">
              <div class="card-title align-items-start flex-column">
                <h3 class="card-label font-weight-bolder text-dark">
                  Personal Information
                </h3>
                <span class="text-muted font-weight-bold font-size-sm mt-1">
                  Update your personal informaiton
                </span>
              </div>
              <div class="card-toolbar">
                <button type="reset" class="btn btn-success mr-2">
                  Save Changes
                </button>
                <button type="reset" class="btn btn-secondary">
                  Cancel
                </button>
              </div>
            </div>

            <form class="form">
              <div class="card-body">
                <div class="row">
                  <label class="col-xl-3"></label>
                  <div class="col-lg-9 col-xl-6">
                    <h5 class="font-weight-bold mb-6">Customer Info</h5>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-xl-3 col-lg-3 col-form-label text-right">
                    Avatar
                  </label>
                  <div class="col-lg-9 col-xl-6">
                    <div
                      class="image-input image-input-outline"
                      id="kt_profile_avatar"
                      style={{
                        backgroundImage: `url(${toAbsoluteUrl(
                          "/media/users/300_21.jpg"
                        )})`,
                      }}
                    >
                      <div
                        class="image-input-wrapper"
                        style={{
                          backgroundImage: `url(${toAbsoluteUrl(
                            "/media/users/300_21.jpg"
                          )})`,
                        }}
                      ></div>

                      <label
                        class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                        data-action="change"
                        data-toggle="tooltip"
                        title=""
                        data-original-title="Change avatar"
                      >
                        <i class="fa fa-pen icon-sm text-muted"></i>
                        <input
                          type="file"
                          name="profile_avatar"
                          accept=".png, .jpg, .jpeg"
                        />
                        <input type="hidden" name="profile_avatar_remove" />
                      </label>

                      <span
                        class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                        data-action="cancel"
                        data-toggle="tooltip"
                        title="Cancel avatar"
                      >
                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                      </span>

                      <span
                        class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                        data-action="remove"
                        data-toggle="tooltip"
                        title="Remove avatar"
                      >
                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                      </span>
                    </div>
                    <span class="form-text text-muted">
                      Allowed file types: png, jpg, jpeg.
                    </span>
                  </div>
                </div>
                <div class=" row">
                  <label class="col-xl-3 col-lg-3 col-form-label text-right">
                    Full Name
                  </label>
                  <div class="col-lg-9 col-xl-6">
                    <input
                      class="form-control form-control-lg form-control-solid"
                      type="text"
                      value={this.state.name}
                    />
                  </div>
                </div>
                <div class="row">
                  <label class="col-xl-3"></label>
                  <div class="col-lg-9 col-xl-6">
                    <h5 class="font-weight-bold mt-10 mb-6">Contact Info</h5>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-xl-3 col-lg-3 col-form-label text-right">
                    Contact Phone
                  </label>
                  <div class="col-lg-9 col-xl-6">
                    <div class="input-group input-group-lg input-group-solid">
                      <div class="input-group-prepend">
                        <span class="input-group-text">ID</span>
                      </div>
                      <input
                        type="text"
                        class="form-control form-control-lg form-control-solid"
                        value={this.state.id}
                      />
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-xl-3 col-lg-3 col-form-label text-right">
                    Email Address
                  </label>
                  <div class="col-lg-9 col-xl-6">
                    <div class="input-group input-group-lg input-group-solid">
                      <div class="input-group-prepend">
                        <span class="input-group-text">@</span>
                      </div>
                      <input
                        type="text"
                        class="form-control form-control-lg form-control-solid"
                        value={this.state.email}
                        placeholder="Email"
                      />
                    </div>
                    <span class="form-text text-muted">
                      We'll never share your email with anyone else.
                    </span>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ProfileInfo;
