import {createSlice} from "@reduxjs/toolkit";
import axios from 'axios'
import api from '../server/orderAPI';
import apiProduct from '../server/productAPI'
const initialOrdersState = {
    listLoading: false,
    actionsLoading: false,
    totalCount: 0,
    entities: null,
    perPage: 0

};
export const callTypes = {
    list: "list",
    action: "action"
};

export const ordersSlice = createSlice({
    name: "orders",
    initialState: initialOrdersState,
    reducers: {
        // getOrder
        OrderSuccess: (state, action) => {
            state.entities = action.payload;
        }

    }
});

const {OrderSuccess} = ordersSlice.actions
export const getOrderList = (params) => async dispatch => {
    try {
        await api.get('orders?' + params).then((res) => {
            var entities = res.data.data || [];
            // var totalCount = res.data.total || 0;
            // var perPage = res.data.per_page || 0;
            const data = {
                entities
            }
            dispatch(OrderSuccess(data));
            
        })

    } catch (e) {
        return console.error(e.message);
    }
}
export const createOrder = (object) => async dispatch => {
    return  api.post('orders',object);
}
export const deleteOrder = (id) => async dispatch => {
        return  api.delete('orders/' + id);
}
export const updateItemOrder = (object) => async dispatch => {
    return  api.put('orders/items/' + object.id,object);
}
export const getOrderById = (id) => async dispatch => {
    return  api.get('orders/' + id+"?with=items.purchase.items;items.tracking;cost;shipmentInfor");
}
export const getProductList=(object)=> async dispatch => {
    if (object){
      return   apiProduct.get('products?search=id:'+object);
    }
    return  apiProduct.get('products')
}
export const getSupplierList=()=> async dispatch => {
    return  apiProduct.get('suppliers')
}
export const getTaxesList=()=> async dispatch => {
    return  apiProduct.get('taxes')
}
export const getShipmentList=()=> async dispatch => {
    return  api.get('shipment-infors')
}
export const saveShipment=(object)=> async dispatch => {
    return  api.post('shipment-infors',object);
}
export const deleteShipment=(id)=> async dispatch => {
    return  api.delete('shipment-infors/'+id);
}
