
import { createSlice } from "@reduxjs/toolkit";
import axios from 'axios'

const initialProductsState = {
    listLoading: false,
    actionsLoading: false,
    totalCount: 0,
    entities: null,
    perPage: 0

};
export const callTypes = {
    list: "list",
    action: "action"
};

export const productsSlice = createSlice({
    name: "products",
    initialState: initialProductsState,
    reducers: {
        // getProducts
        Productsuccess: (state, action) => {
            state.perPage = action.payload;
            state.totalCount = action.payload;
            state.entities = action.payload;
        }

    }
});
const api = axios.create({
    'baseURL': 'http://139.180.207.4:84/api/',
    headers: {
        'Authorization': `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiNjYzZTQ0ZjQ5OTJkNTY5NjNjYmRjMjhmOTAxMTJhYjk3NzE0ZDcwNDg3NTRiYzA1NzVlOGI2ZTEyOTc1YTYxNDNkOWI2MGQ5ZDU5NDdkZGEiLCJpYXQiOjE1OTY2MTAyNTMsIm5iZiI6MTU5NjYxMDI1MywiZXhwIjoxNTk3OTA2MjUzLCJzdWIiOiJhZG1pbiIsInNjb3BlcyI6WyIqIl19.aXWzU-dudGPWnvPC-9u4T49fkUeGrWWDaYxc1g4W8Jw4ZyO8caeBTW3sbXXsL3GrEcurnNvirXtbZP5fjPq_woWqFU26-8IgLHG7nkSxuV_QxEuTmFAgYfZs8sGdF9XCB5ZpKUvLwL2-o4Tqq9zIn3Hkg3pfmc7Dy0tfJRTtmIMa6OdjI7ATpK_yshAJ4ugWCgJCoBUNLUCjoMmNxqY-KXWy12DsPicfatrnvDdIIcMJ4igDPe0QUcWFcDaIOSYQPSTIU-C-T86nOLM-Fdkmqvn-0tj5BXCGN-KOdwer1vNtglLAwmIF1w8q4lgJaa5CeG27xB6KfSdx9aZ5xA0Le2rifvCEVfK0o2S2Hgz98u8k7fLliLw4Usgz_6ezb5c6u-Z9qBXNDVf9h01gK3x1eowgiqN3RZxe9yrKUf_Cy_F6emECmEGGuirmeV1I-JIFJs5vngIXZBemapce2dsHpRv9bvEE_Bl6H4YhmBGoOjFiFOzfKg3vK1XbkJEQJBUouBEoc2bRu-ewvLiYhiKdGhePllhT9q8TP1eeatcIrnq8ZF5luDUBeRbwpY1595bnl28zdZwOKvSQD12qnCe5f0dBFI4FLXLjxhJxqHvbB0U7P2IkNEDbrKVd_Cf-5-mX89KbsOsFzH7j1IdXsV54088NhvMLLWHJH-JlStfTxYM`
    }
})
const { Productsuccess } = productsSlice.actions
export const getProductsList = () => async dispatch => {
    try {
        await api.get('products?').then((res) => {
            var entities = res.data.data || [];
            var totalCount = res.data.total || 0;
            var perPage = res.data.per_page || 0;
            const data = {
                entities,
                totalCount,
                perPage
            }
            dispatch(Productsuccess(data));

        })

    } catch (e) {
        return console.error(e.message);
    }
}
export function getProduct(id) {
    return api.get('products/' + id);
}
export const deleteProducts = (id) => async dispatch => {
    return api.delete('products/' + id);
}
export const updateProducts = (name, locale, ingredients) => async dispatch => {
    return api.put('products/' + `name=${name}&locale=${locale}&ingredients=${ingredients}`);
}
export const createProducts = (object) => async dispatch => {
    return api.post('products', object)
}
